/* Imports *****************************************************************************************************************************************/
import joplin from 'api';
import { noteUpdateHandler, updateAllRecurrences } from './core/recurrence';
import { setupDatabase,getRecord, updateRecord } from './core/database';
import { connectNoteChangedCallback,getNote,updateNoteBody,setTaskDueDate } from './core/joplin';
import { setupDialog } from './gui/dialog';
import { setupControls } from './gui/controls';
import format from 'date-fns/format';
import { setStatistics } from './core/statistics';

/** Plugin Registration *****************************************************************************************************************************
 * Registers the plugin with joplin.                                                                                                                *
 ***************************************************************************************************************************************************/
joplin.plugins.register({
    onStart: main,
});

/** Main ********************************************************************************************************************************************
 * Calls all the functions needed to initialize the plugin                                                                                          *
 ***************************************************************************************************************************************************/
async function main() {
    await setupDatabase()
    await setupDialog()
    await setupControls()
    await updateAllRecurrences()
    await connectNoteChangedCallback(noteUpdateHandler)
    await joplin.workspace.onNoteChange(noteUpdateHandler)
	
	// when the due date is arrived, write the note
	joplin.workspace.onNoteAlarmTrigger(updateTodoInfo)
}

/** updateTodoInfo *************************************************************************************************************************************
 * If the given todo has not been completed and has a due date and recurrence is enable, and the todo due date is out of date *
 * write the to do note with the unfinished task and the due date is changed to the next due date*
 *                                                                                                                                  *
 ***************************************************************************************************************************************************/
async function updateTodoInfo(event){
	//logging.info("Update note: " + event.noteId);
	var todoID = event.noteId;
	var todo = await getNote(todoID)
	var recurrence = await getRecord(todoID)
	if ((todo.todo_completed != 1) && (recurrence.enabled)){
		var initialDate = new Date(todo.todo_due)
        var nextDate = recurrence.getNextDate(initialDate)
        await setTaskDueDate(todoID, nextDate)
		recurrence.updateStopStatus()
		updateRecord(todoID, recurrence)
	
		// update the todo note: add unfinished task to the note in the end
		var newDate = new Date(todo.todo_due)
		var dateStr = format(newDate, 'yyyy-MM-dd HH:mm:ss')
		var finishedTask = "- [ ] " + dateStr + " " + todo.title
		todo.body = todo.body + "\n"+ finishedTask
		await setStatistics(todo.body, todoID)
	}
}